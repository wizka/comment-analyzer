
import requests
import json

from comment_analyzer import settings

class EmotionMixin(object):

    def manage_response_from_beto_analysis(self, response):
        analysis = None

        if (response.ok):
            analysis = response.json()

        return {
            'analysis': analysis,
            'status_code': response.status_code,
            'reason': response.reason
        }

    def beto_emotion_analysis(self, input):
        url = settings.HUGGINGFACE_API_URL + settings.HUGGINGFACE_API_MODEL
        headers = { "Authorization": "Bearer " + settings.HUGGINGFACE_API_KEY }
        json_dict = { "inputs": input }
        json_obj = json.dumps(json_dict)
        response = requests.post(
            url=url,
            headers=headers,
            json=json_obj
        )
        return self.manage_response_from_beto_analysis(response)
