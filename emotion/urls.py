# Django imports
from django.urls import path

# Views import
from emotion import views

urlpatterns = [
    path(
        'comment-analysis',
        views.CommentAnalysisRetrieveAPIView.as_view(),
        name='comment_analisys'
    ),
]
