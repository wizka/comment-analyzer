from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate

from emotion.views import CommentAnalysisRetrieveAPIView

from authentication.tests import authenticated_get_request

class CommentAnalysisRetrieveAPIViewTest(TestCase):

    class_view = CommentAnalysisRetrieveAPIView
    url_request = '/api/emotion/comment-analysis'
    phrase = 'Te quiero. Te amo.'

    def test_comment_analysis_retrieve_assert_ok(self):
        """
            Comment list works correctly.
        """
        auth_req = authenticated_get_request(self.url_request+ '?input_text=' + self.phrase)
        view = self.class_view.as_view()
        force_authenticate(auth_req['request'], user=auth_req['user'])
        response = view(auth_req['request'])
        self.assertEqual(response.status_code, 200)
