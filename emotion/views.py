
from emotion.mixins import EmotionMixin
from rest_framework.generics import RetrieveAPIView

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

permission_classes = [IsAuthenticated]

class CommentAnalysisRetrieveAPIView(RetrieveAPIView, EmotionMixin):
    """
        Retrieve analysis data.
    """
    def retrieve(self, request, *args, **kwargs):
        input_text = request.GET.get('input')
        analysis = self.beto_emotion_analysis(input=input_text)
        return Response(
            data=analysis['analysis'],
            status=analysis['status_code'],
        )


