"""
URL configuration for comment_analyzer project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

# Apps routes
from authentication import urls as authentication_urls
from comment import urls as comment_urls
from emotion import urls as emotion_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/authentication/', include((authentication_urls, 'authentication'), namespace='authentication')),
    path('api/comment/', include((comment_urls, 'comment'), namespace='comment')),
    path('api/emotion/', include((emotion_urls, 'emotion'), namespace='emotion')),
]
