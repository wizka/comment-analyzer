# Django libs
from django.contrib.auth.models import User

# Django rest framework
from rest_framework.generics import CreateAPIView
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

# Authentication
from authentication.serializers import UserAuthTokenSerializer, UserDeleteAuthTokenSerializer, UserModelSerializer

class UserRegisterCreateAPIView(CreateAPIView):
    """
        Create new user.
    """
    model = User
    serializer_class = UserModelSerializer

class UserObtainAuthToken(ObtainAuthToken):
    """
        Obtain token for Bearer token.
    """
    serializer_class = UserAuthTokenSerializer

class UserDeleteAuthToken(ObtainAuthToken):
    """
        Delete token.
    """
    serializer_class = UserDeleteAuthTokenSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        request.user.auth_token.delete()
        return Response( { 'message' : 'Log out.'} )
