# Django imports
from django.urls import path

# Views import
from authentication import views

urlpatterns = [
    path(
        'register',
        views.UserRegisterCreateAPIView.as_view(),
        name='register'
    ),
    path(
        'login',
        views.UserObtainAuthToken.as_view(),
        name='login'
    ),
    path(
        'logout',
        views.UserDeleteAuthToken.as_view(),
        name='logout'
    )
]
