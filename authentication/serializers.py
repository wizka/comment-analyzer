# Django libs
from django.contrib.auth.models import User

# Django rest framework
from rest_framework import serializers
from rest_framework.authtoken.serializers import AuthTokenSerializer

class UserModelSerializer(serializers.ModelSerializer):
    """
        Serializer for new users.
    """
    password1 = serializers.CharField(
        style={'input_type': 'password'}, write_only=True
    )
    password2 = serializers.CharField(
        style={'input_type': 'password'}, write_only=True)
    class Meta():
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        ]

    def validate(self, attrs):
        """
            Passwords must match.
        """
        p1 = attrs.get('password1')
        p2 = attrs.get('password2')
        if p1 != p2:
            raise serializers.ValidationError(
                {'password': 'Passwords must match'}
            )
        return attrs
    
    def create(self, validated_data):
        """
            Create user and set password.
        """
        user = User.objects.create(
            username=self.validated_data['username'],
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            email=self.validated_data['email'],
            is_active=True
        )

        user.set_password(validated_data['password1'])
        user.save()

        return user
    

class UserAuthTokenSerializer(AuthTokenSerializer):
    """
        Serializer for obtain Authentication Token.
    """
    pass

class UserDeleteAuthTokenSerializer(serializers.Serializer):
    """
        Serializer delete token.
    """
    id = serializers.ReadOnlyField()
    class Meta:
        model = User
