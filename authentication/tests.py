from django.test import TestCase
from rest_framework.test import APIRequestFactory
from django.contrib.auth.models import User

# Create your tests here.

def authenticated_get_request(url):
    factory = APIRequestFactory()
    user = User.objects.create(
        username='test',
        first_name='test',
        last_name='er',
        email='tester@test.test',
        is_active=True
    )
    user.set_password('test123testdjango')
    user.save()
    request = factory.get(url)
    return {
        'user': user,
        'request': request,
    }
