# Django imports
from django.urls import path

# Views import
from comment import views

urlpatterns = [
    path(
        'list',
        views.CommentListAPIView.as_view(),
        name='list'
    ),
]
