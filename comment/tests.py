from django.test import TestCase
from rest_framework.test import APIRequestFactory, force_authenticate

from comment.views import CommentListAPIView

from authentication.tests import authenticated_get_request



class CommentListAPIViewTest(TestCase):

    class_view = CommentListAPIView
    url_request = '/api/comment/list'

    def test_comment_list_api_view_assert_unauthorized(self):
        """
            Comment list returns unauthorized.
        """
        factory = APIRequestFactory()
        view = self.class_view.as_view()
        request = factory.get(self.url_request)
        response = view(request)
        self.assertEqual(response.status_code, 401)

    def test_comment_list_api_view_assert_ok(self):
        """
            Comment list works correctly.
        """
        auth_req = authenticated_get_request(self.url_request)
        view = self.class_view.as_view()
        force_authenticate(auth_req['request'], user=auth_req['user'])
        response = view(auth_req['request'])
        self.assertEqual(response.status_code, 200)
