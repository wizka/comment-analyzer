import pandas as pd
from rest_framework.generics import ListAPIView

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

class CommentListAPIView(ListAPIView):
    """
        Comments list loaded from csv.
    """
    csv_dataset_file = './datasets/DataRedesSociales'
    usecols = ['text', 'likes', 'comments', 'shares']
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        """
            list method overwritten.
        """
        filename_with_extension = self.csv_dataset_file + '.csv'
        data_frame = pd.read_csv(filename_with_extension, usecols=self.usecols)
        comments = []
        for comment_obj in data_frame.iloc:
            comment = {
                "text":comment_obj.values[0],
                "likes":comment_obj.values[1],
                "comments":comment_obj.values[2],
                "shares":comment_obj.values[3],
            }
            comments.append(comment)
        response = comments
        return Response({"data": response})
    